;;; jump-last.el --- Jump to the Last Edit Location, regardless of the file.  -*- lexical-binding: t; -*-

;; Copyright (C) 2020,2023,2024 Rostislav Svoboda

;; Authors: Rostislav Svoboda <Rostislav.Svoboda@gmail.com>
;;          Daniel Nicolai <dalanicolai@gmail.com>
;; Version: 0.1
;; Package-Requires: ((goto-chg "1.7.5"))
;; Keywords:
;; URL: https://github.com/Bost/jump-last

;;; TODO extend with functionality for jumping to closed, i.e. killed buffers

;;; Installation:
;; In the `dotspacemacs/user-config', add there:
;;   (use-package jump-last)
;; then, in the `dotspacemacs-additional-packages', add there:
;;   (jump-last :location
;;              (recipe :fetcher github :repo "Bost/jump-last"))
;; or:
;;   $ git clone https://github.com/Bost/jump-last
;; and then
;;   (jump-last :location "<path/to/the/cloned-repo>")

(require 'cl-lib)
(require 'goto-chg)

;; (defvar-local ctx "[jump-last]" "Log message context")

(defvar jl-last-edited-buffers '()
  "List of last edited buffers")

(defun jl-last-edited-buffer-changed-p ()
  "See also `window-buffer', `buffer-name' and
  `buffer-file-name', however the `buffer-file-name' returns nil
  when in the *scratch* buffer."
  (let ((curr-buff (current-buffer)))
    (and
     ;; (car '()) => nil; (cdr '()) => nil
     (not (equal (car jl-last-edited-buffers) curr-buff))
     (not (member curr-buff
                  (list " *Geiser font-lock*"
                        " markdown-code-fontification:clojure-mode"))))))

(defun jl-save-last-edited-buffer (beg end length)
  "Hooked to `after-change-functions', which requires functions of
three arguments. See `after-change-functions'

See also `window-buffer', `buffer-name' and
  `buffer-file-name', however the `buffer-file-name' returns nil
  when in the *scratch* buffer."
  (ignore beg end length) ;; prevent misleading compilation warning
  (when (jl-last-edited-buffer-changed-p)
    ;; push unless (current-buffer) is already in the list
    (cl-pushnew (current-buffer) jl-last-edited-buffers)))

(defun jl-remove-killed-buffer ()
  (let ((curr-buff (current-buffer)))
    (setq jl-last-edited-buffers
          (cl-remove-if (lambda (e) (eq e curr-buff))
                        jl-last-edited-buffers))))

(defun jl-jump-last-edited-place ()
  (interactive)
  (when-let ((last-buffer (car jl-last-edited-buffers)))
    (when (jl-last-edited-buffer-changed-p)
      (pop-to-buffer last-buffer)))
  ;; Go to the point where the last edit was made in the current buffer. Repeat
  ;; the command to go to the second last edit, etc.
  (goto-last-change nil))

(defun jl-register-hooks ()
  (when (derived-mode-p 'text-mode 'prog-mode)
    ;; (message "[jl-register-hooks] major-mode: %s; adding hook)"
    ;;          major-mode)

    ;; 'after-change-functions
    ;; List of functions to call after each text change.
    (add-hook 'after-change-functions #'jl-save-last-edited-buffer)
    ;; TODO It might be calle with additional 0 t arguments:
    ;; (add-hook 'after-change-functions #'jl-save-last-edited-buffer 0 t)
    (add-hook 'kill-buffer-hook #'jl-remove-killed-buffer)))

;; (remove-hook 'after-change-functions #'jl-save-last-edited-buffer)
;; (remove-hook 'kill-buffer-hook #'jl-remove-killed-buffer)

(dolist (hook '(text-mode-hook prog-mode-hook))
  (add-hook hook #'jl-register-hooks)
  ;; (remove-hook hook #'jl-register-hooks)
  )

;; Map it to e.g. `WinKey + F10':
;;   (global-set-key (kbd "s-<f10>") #'jl-jump-last-edited-place)
;; or remap it completely if you're brave enough:
;;  (global-set-key [remap #'goto-last-change] #'jl-jump-last-edited-place)

(provide 'jump-last)
